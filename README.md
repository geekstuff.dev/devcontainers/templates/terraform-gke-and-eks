# Devcontainers Templates - Terraform GKE & EKS

This project is a [Devcontainer](.devcontainer/) template built against both
`debian` and `ubuntu` images of the
[Terraform GKE template](https://gitlab.com/geekstuff.dev/devcontainers/templates/terraform-gke).

It adds AWS cli.

## How it's used

The .devcontainer/ configuration in this project can be used with simple entries
in your own `.devcontainer/devcontainer.json` file where you can further customize
it for example:

```json
{
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/templates/terraform-gke-and-eks/debian/bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/docker": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/vault-cli": {}
    }
}
```

From the example above:

- In this example, image `debian/bullseye` can be changed to `ubuntu/20.04`.
- Image can also have a tag, `:latest` for the main branch, or `:vX.Y` from tags in this project.
    - See [here for the docker image list](https://gitlab.com/geekstuff.dev/devcontainers/templates/terraform-gcp/container_registry)
- While the image comes with the latest terraform, it can also re-use terraform feature and
  pin a specific version.
- That example also wanted Docker and Vault CLIs.
